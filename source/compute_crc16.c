/*
 * compute_crc16.c
 *
 *  Created on: 17 de jun de 2018
 *      Author: Maique Garcia
 */


//#include <stdio.h>
//#include "c_37118_defines.h"
//#include <stdlib.h>
#include "./include/compute_crc16.h"
#include <stdint.h>


uint compute_crc16(unsigned char *message, uint MessLen){

    register uint16_t crc = 65535;//0xffff; // crc initial value.
    register uint16_t temp; //temporary value.
    register uint16_t quick;
    register uint16_t i; //for iteration loop

    for(i=0;i<(uint16_t)MessLen;i++){
        //printf("message i %hX \n",message[i]);
        temp = (crc>>8) ^ message[i];
        crc<<=8;
        quick = temp ^ (temp>>4);
        crc ^= quick;
        quick <<=5 ;
        crc ^= quick;
        quick <<=7 ;
        crc ^= quick;
        //printf("crc obtained is %hX \n",crc);
    };//end of for
    return (uint)crc;

};
