/*
 * c_37118_protocol.h
 *
 * Version: 1.0
 * Date:	18.10.2013
 *
 */

#ifndef _C_37118_PROTOCOL_H
#define _C_37118_PROTOCOL_H

/****************************************************************
 * Includes
 ****************************************************************/
#include <stdint.h>
#include "./include/compute_crc16.h"

/****************************************************************
 * Parameters for protocol program
 ****************************************************************/
#define C_FREQ_DFREQ 1 // 0 for 16 bit integer(2 bytes) / 1 for float point(4 bytes)
#define C_ANALOG_TYPE 0 // 0 for 16 bit integer / 1 for float point
#define C_PHASOR_TYPE 1 // 0 for 16 bit integer / 1 for float point
#define C_PHASOR_REPRESENT 0 // 0 for complex representation / 1 for float polar representation

//The Analog channels amount
#define C_ANNMR 0 //For now, no analog phases
//The number of digital channels
#define C_DGNMR 0

//Path to FIFO - PMU2C37118
#define C_FIFO_PATH_SERVER "/tmp/pmu2C37_118/"
#define C_FIFO_PATHNAME_SERVER "/tmp/pmu2C37_118/pmu2c37"  /* Path used on ftok for shmget key  */

//Leading byte to send
#define C_LEADING_BYTE 0xAA //170 //AA

//Frame types
#define C_CFG_2 3
#define C_HDR 1
#define C_DATA_FRAME 0

//Frame c
#define C_PMU_FRAME_VERSION 2

//Nominal Line frequency code flags 0
#define C_FNOM_SIXTY 0 // 15:1 = Reserv, 0 = (1 to 50 hz, 0 to 60hz)
#define C_FNOM_FIFTY 1 // 15:1 = Reserv, 0 = (1 to 50 hz, 0 to 60hz)

//Configuration change
#define C_CFGCNT 0 //initial factory config = 0

//Characters constants
#define C_CHAR_A 65//A
#define C_CHAR_P 80 //P
#define C_CHAR_a 97 //a
#define C_CHAR_h 104 //h
#define C_CHAR_s 115 //s
#define C_CHAR_o 111 //o
#define C_CHAR_r 114 //r
#define C_CHAR_q 113 //q
#define C_CHAR_e 101 //e
#define C_CHAR_S 83 //S
#define C_CHAR__ 95 //_
#define C_CHAR_SPACE 32 //
#define C_CHAR_B 66 //B
#define C_CHAR_C 67 //C

//PHSCALE for positive seq
#define PHSCALE_POSS_BT12 416 //see table 11_partB to discorver
#define PHSCALE_POSS_BT3 1 //voltage + positive seq
#define PHSCALE_POSS_BT4 0 //User designation
#define PHSCALE_POSS_4BYT2 0 //second 4bytes word - Mag compensation
#define PHSCALE_POSS_4BYT3 1 //third 4bytes word - Angle compensation
//PHSCALE for Voltage
#define PHUNIT_POSS_BYT4 0 //Mag & angle compensation
#define PHUNIT_POSS_BYT3 0 //00 //Mag & angle compensation
#define PHUNIT_POSS_BYT2 1 //second 4bytes word - Mag & angle compensation
#define PHUNIT_POSS_BYT1 141 //third 4bytes word - Mag & angle compensation
//PHSCALE for Current
#define PHUNIT_CURRNT_BYT4 1 //Mag & angle compensation
#define PHUNIT_CURRNT_BYT3 0 //00 //Mag & angle compensation
#define PHUNIT_CURRNT_BYT2 1 //second 4bytes word - Mag & angle compensation
#define PHUNIT_CURRNT_BYT1 141 //third 4bytes word - Mag & angle compensato

#pragma pack(push,1)
typedef struct
{
	uint16_t leading_byte   : 8;
	uint16_t reserved_bit   : 1;
	uint16_t frame_type     : 3;
	uint16_t version_number : 4;
} sync_fields_st;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{
	sync_fields_st	hdr_sync;
	uint16_t		hdr_framesize;
	uint16_t		hdr_id_code;
	uint32_t		hdr_soc_field;
	uint32_t		hdr_fracsec;
	char			*hdr_data;
	uint16_t		hdr_crc;
} header_frame_st;
#pragma pack(pop)

#endif /* _C_37118_PROTOCOL_H */
