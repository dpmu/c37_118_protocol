/*
 * decode_D-PMU_file_parameters.c
 *
 *  Created on: 17 de jun de 2018
 *      Author: Maique Garcia
 */

#include "./include/parameters_decoder.h"
//#include <stdint.h>
#include <stdio.h>

#define C_PARAMETER_FILE "/usr/D-PMU_parameters/D-PMU_parameters.txt"

struct dpmu_file_decoded_st interpreted_data; //store data interpreted from the input file.
uint8_t send_data_other_PDC; //Default value of this flag is zero

void decode_DPMU_file_parameters(void){

	FILE * fp;
	char * line = NULL;
	size_t len = 0;
	size_t length, length_header;

	//Reset values to variables
	send_data_other_PDC = 0;

	//Open file
	fp = fopen(C_PARAMETER_FILE, "r");
	//check file health
	if (fp == NULL)
	{
		printf("Error. The Parameters file is empty.");
		exit(EXIT_FAILURE);
	}

	//structure reset.
	memset(&interpreted_data, 0x00, sizeof(interpreted_data));

	//Read and decode important lines (with '*' identifier )
	while ((length = getline(&line, &len, fp)) != -1) //seek file until it ends
	{

		if(line[0] == '*') //parameter identifier
		{
			if(strncmp(line,"*D-PMU_NAME",11) == 0)
			{
				interpreted_data.dec_name.name_length = getline(&interpreted_data.dec_name.name, &len, fp) - 1; //take the value from D-PMU name. Minus 1 to eliminate \n character
				printf("D-PMU name: %s\r\n", interpreted_data.dec_name.name);
			}
			else if(strncmp(line,"*PDC_IP",7)== 0)
			{
				length = getline(&interpreted_data.pdc_ip, &len, fp); //take the value from PDC destiny ip
				printf("Primary PDC IP: %s\r\n", interpreted_data.pdc_ip);
			}
			else if(strncmp(line,"*PDC_PORT",9)== 0)
			{
				length = getline(&interpreted_data.pdc_port, &len, fp); //take the value from PDC destiny port
				printf("Primary PDC port: %s\r\n", interpreted_data.pdc_port);
			}
			else if(strncmp(line,"*SEND_DATA_OTHER_PDC",20) == 0)
			{
				length = getline(&line, &len, fp); //take response from ask to send to other PDC
				if(strncmp(line,"yes",3) == 0)
				{
					send_data_other_PDC = 1;
				}
			}
			else if((strncmp(line,"*SCND_PDC_IP",12) == 0) && (send_data_other_PDC == 1))
			{
				length = getline(&interpreted_data.scnd_pdc_ip, &len, fp); //take the value from secondary PDC destiny ip
				printf("Secondary PDC IP: %s\r\n", interpreted_data.scnd_pdc_ip);
			}
			else if((strncmp(line,"*SCND_PDC_PORT",14)== 0)  && (send_data_other_PDC == 1))
			{
				length = getline(&interpreted_data.scnd_pdc_port, &len, fp); //take the value from secondary PDC destiny port
				printf("Secondary pdc port: %s\r\n", interpreted_data.scnd_pdc_port);
			}
			else if(strncmp(line,"*D-PMU_HEADER",13) == 0)
			{
				interpreted_data.dec_header.header_length = getline(&interpreted_data.dec_header.header, &len, fp) - 1; //take the header Minus 1 to eliminate \n character
				printf("HEADER to D-PMU: %s\r\n", interpreted_data.dec_header.header);
			}
			else if(strncmp(line,"*D-PMU_IDCODE",13) == 0)
			{
				length = getline(&interpreted_data.dpmu_idcode, &len, fp); //take the header Minus 1 to eliminate \n character
				printf("IDCODE to D-PMU: %s\r\n", interpreted_data.dpmu_idcode); //printf("IDCODE da D-PMU: %c\r\n", interpreted_data.dpmu_idcode[0]);
			}
			else if(strncmp(line,"*NUMBER_OF_PHASORS_TO_SEND",26)== 0)
			{
				length = getline(&line, &len, fp); //take the value from PDC destiny ip
				//printf("Number of phasors to send: %s\r\n", line);
				if(strncmp(line,"7",1) == 0)
				{
					interpreted_data.number_of_phasors = 7;
				}
				else if(strncmp(line,"6",1) == 0)
				{
					interpreted_data.number_of_phasors = 6;
				}
				else
				{
					interpreted_data.number_of_phasors = 3;
				}
			}
			else if (strncmp(line,"*NOMINAL_SYS_FREQUENCY",22) == 0)
			{
				length = getline(&line, &len, fp);
				interpreted_data.nominal_sys_freq = atoi(line);
				printf("Nominal configured frequency: %d\r\n", interpreted_data.nominal_sys_freq);
			}
			else if(strncmp(line,"*INTERNET_PROTOCOL",18)== 0) //Protocol used to send data to PDC
			{
				interpreted_data.communication_protocol = 0; //1 =TCP; 0 = UDP (default)
				length = getline(&line, &len, fp);
				if(strncmp(line,"TCP",3) == 0)
				{
					interpreted_data.communication_protocol = 1; //1 =TCP; 0 = UDP
				}
			}

			else
			{

			}
		}

	}
	puts("--------------------processo de decodificacao ENCERRADO--------------------------------");

	fclose(fp);
	//if (line)
	//	free(line);
	//exit(EXIT_SUCCESS);

};

dpmu_dec_name_st return_dpmu_name(void)
{
	dpmu_dec_name_st result;

	result.name = interpreted_data.dec_name.name;
	result.name_length = interpreted_data.dec_name.name_length;

	return result;
}



dpmu_dec_header_st return_dpmu_header(void)
{
	dpmu_dec_header_st result;

	result.header = interpreted_data.dec_header.header;
	result.header_length = interpreted_data.dec_header.header_length;

	return result;
}


/*Function  char *return_dpmu_idcode(void)
 * Brief	This function get the D-PMU IDCODE from the parameter file.
 * Param	none
 * Return 	uint16_t
 */
uint16_t return_dpmu_idcode(void)
{
	return atoi(interpreted_data.dpmu_idcode);
}


char *return_dpmu_pdc_destinyip(void)
{
	return interpreted_data.pdc_ip;
}



char *return_dpmu_pdc_destinyport(void)
{
	return interpreted_data.pdc_port;
}



/* Return the status of configuration to secondary PDC IP*/
//Return 1 to enabled secondary IP
//Return 0 to disabled secondary IP
uint8_t return_dpmu_enabled_scnd_pdc(void)
{
	if (send_data_other_PDC == 1)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


//Call that only if secndary ip is enabled.
char *return_dpmu_scnd_pdc_destinyip(void)
{
	return interpreted_data.scnd_pdc_ip;
}



char *return_dpmu_scnd_pdc_destinyport(void)
{
	return interpreted_data.scnd_pdc_port;
}

uint8_t return_dpmu_number_of_phasors(void)
{
	if (interpreted_data.number_of_phasors == 0)
	{
		return 3; //Default value
	}
	else
	{
		return interpreted_data.number_of_phasors; //Parameter decoded value
	}
}

uint8_t return_dpmu_internet_protocol(void)
{
	return interpreted_data.communication_protocol; //internet protocol
}

uint16_t return_dpmu_sys_freq(void)
{
	return interpreted_data.nominal_sys_freq;
}
