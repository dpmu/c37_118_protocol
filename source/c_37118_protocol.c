/*
 * c_37118_protocol.c
 *
 * Version: 1.0
 * Date:	17.06.2018
 *
 *
 * In Eclipse add Include path
 *     C:\gcc-linaro\arm-linux-gnueabihf\libc\usr\include
 *
 */

/*
Version Control
// Vr number     /   Date   / Description
//1.0			/ 07-07-18  / Working version to BBB
//1.1			/ 09-07-18	/ Changed the main to accept the "S" or "N" parameters inputs. (send to UFSC parameter).
//1.2			/ 06-08-18  / CHANGING the frequency and ROCOF from fixed to float type variables (changing format filed too)
//1.3			/ 17-08-18	/ Adding the data flow to UNICAMP PDC (IP: 192.168.0.67)
//1.4			/ 22-01-19  / improvement to performance.
//1.5			/ 04-07-19	/ Add IP as a input parameter (on parameter file).
//1.6			/ 22-03-19  / Added parameter decoder capability through parameters_decoder module
//1.7			/ 28-11-20	/ Added support for 50Hz grid systems - Framerate will be the same as grid frequency
//1.8			/ 20-05-21	/ Added support for TCP or UDP selection protocol on parameter file
*/

//Fonte: http://jkuhlm.bplaced.net/hellobone/
#include "../include/c_37118_protocol.h"
#include "../include/parameters_decoder.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h> //used to interpret the O_RDONLY parameter

//IP internet used libraries
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <syslog.h> //sys log file rotation If is too shifted (no occurrence of DPMU logs), use journalctl to see logs.


//See values on
//cat /var/log/syslog | grep Samples_handler
//write on file in this form '   syslog(LOG_ERR, "MAIQUE TESTE Syslog", valuesss )  ';

//Errno status var
extern int errno;

//Structures frame pointers
header_frame_st header_frame;

int main() {
	puts("PMU to PDC Protocol Processing. vr 1.8");
	syslog(LOG_INFO, "Running the PMU Protocol program version %f", 1.8);


	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 							Variables and structures for this program									//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Variables for c_37118 structure
	int status_socket_PDC_A, status_socket_PDC_B ;
	char *serv2send_ipAddr = NULL;
	char *serv2send_scndry_ipAddr = NULL;
	char *serv2send_port =0;
	char *serv2send_scndry_port =0;
	uint8_t send_other_PDC_flag;
	dpmu_dec_name_st dpmu_name;
	dpmu_dec_header_st dpmu_header_info;
	uint16_t dpmu_idcode_dec;
    uint8_t number_of_phasors_to_send;
    uint8_t fifo_size_in_bytes;
	uint8_t send_by_UDP_or_TCP=0; //0=UDP, 1=TCP
	uint8_t frame_word[1000];
	uint16_t fr_index = 1;
	uint crc16_obtained;
	uint16_t i;
	uint16_t _nominal_freq = 60;
	uint16_t _nominal_freq_flag = 0; //(1 to 50 hz, 0 to 60hz)

	uint8_t FORMAT_MSB = 0, FORMAT_LSB = (C_FREQ_DFREQ<<3)+(C_ANALOG_TYPE<<2)+(C_PHASOR_TYPE<<1)+(C_PHASOR_REPRESENT);
	uint8_t ANNMR_MSB=C_ANNMR>>8, ANNMR_LSB = C_ANNMR, DGNMR_MSB = C_DGNMR>>8, DGNMR_LSB = C_DGNMR;

	//Variables from
	//PMU to C37118 fifo
	int pmu_pipe_fd;
	int res;
	int open_mode = O_RDONLY;//O_WRONLY;

	//PDCs sockets
	struct sockaddr_in server_PDC_A, server_2;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 							Open and mount C37118 parameters structure									//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	decode_DPMU_file_parameters();

	//Take parameters from parameters file:
	serv2send_ipAddr = return_dpmu_pdc_destinyip(); // PDC IP
	serv2send_port = atoi(return_dpmu_pdc_destinyport()); // PDC PORT
	send_other_PDC_flag = return_dpmu_enabled_scnd_pdc(); //Get the flag to identify the secondary IP
	if (send_other_PDC_flag == TRUE)
	{
		serv2send_scndry_ipAddr = return_dpmu_scnd_pdc_destinyip(); //Secondary PDC IP
		serv2send_scndry_port = atoi(return_dpmu_scnd_pdc_destinyport()); //Secondary PDC PORT
	}
	dpmu_name = return_dpmu_name();
	dpmu_header_info = return_dpmu_header();
	dpmu_idcode_dec = return_dpmu_idcode();
	number_of_phasors_to_send = return_dpmu_number_of_phasors();
	if (number_of_phasors_to_send == 7)
	{
		fifo_size_in_bytes = 52;
	}
	else
	{
		fifo_size_in_bytes = 44;
	}

	_nominal_freq = return_dpmu_sys_freq(); //Get system nominal frequency
	if (_nominal_freq == 60)
	{
		_nominal_freq_flag = C_FNOM_SIXTY;
	}
	else
	{
		_nominal_freq_flag = C_FNOM_FIFTY;
	}
	//Get the internet protocol (0 to UPD, 1 to TCP)
	send_by_UDP_or_TCP=return_dpmu_internet_protocol();

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 							Pointer for specific protocol fields										//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//-----------------------------------------------------------------------------------------------
	uint8_t *IDCODE_pointer 					= 	(uint8_t*)&(dpmu_idcode_dec);
	uint8_t *fr_index_pointer 					= 	(uint8_t*)&(fr_index);
	uint8_t *crc16_pointer 						= 	(uint8_t*)&(crc16_obtained);

	uint8_t pmu_buffer[fifo_size_in_bytes];

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 								Create socket pipe for PDC 1											//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
    puts("opening socket A.");

	if (send_by_UDP_or_TCP)
	{
		status_socket_PDC_A = socket(AF_INET , SOCK_STREAM , IPPROTO_TCP); //TCP WORK

		//todo: find something related with keep alive for keep the socket open even when receiving a TCP FIN command frame. This frame closes the socket on D-PMU and
		//stop D-PMU operation. After 10 minutes, check status will call all programs again.
		//https://stackoverflow.com/questions/283375/detecting-tcp-client-disconnect
	}
	else
	{
		status_socket_PDC_A = socket(AF_INET , SOCK_DGRAM , IPPROTO_UDP);//UDP
	}
    puts("socket open.");


	if (status_socket_PDC_A == -1)
	{
		printf("Could not create socket with PDC 1 - c_37118 program");
		syslog(LOG_ERR, "Error on creating a socket A with PDC with status %d", status_socket_PDC_A);
	}

	server_PDC_A.sin_addr.s_addr = inet_addr(serv2send_ipAddr);//("192.168.1.203"); //Always Verify the Server IP (change every time that i connect my PC on the widora.
	server_PDC_A.sin_family = AF_INET;
	server_PDC_A.sin_port = htons(serv2send_port); //was htons //Carefull with firewall restrictions and, if necessary, create an exception to allow this port and the PDC program

    puts("wite struct to serverA.");


	//Connect to remote server PDC A
	if (connect(status_socket_PDC_A , (struct sockaddr *)&server_PDC_A , sizeof(server_PDC_A)) < 0)
	{
		puts("Socket connect ERROR with PDC 1 - c_37118 program");
		syslog(LOG_ERR, "Error on connecting a socket A to PDC.");
		return 1;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 								Create socket pipe for PDC 2											//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
    puts("opening socket B.");

	 if (send_other_PDC_flag == TRUE)
	 {
		if (send_by_UDP_or_TCP)
		{
			status_socket_PDC_B = socket(AF_INET , SOCK_STREAM , IPPROTO_TCP);//TCP
		}
		else
		{
			status_socket_PDC_B = socket(AF_INET , SOCK_DGRAM , IPPROTO_UDP);//UDP
		}

		 if (status_socket_PDC_B == -1)
		 {
			 printf("Could not create socket with PDC 2 - c_37118 program");
			 syslog(LOG_ERR, "Error on creating a socket B with PDC with status %d", status_socket_PDC_B);
		 }
		 server_2.sin_addr.s_addr = inet_addr(serv2send_scndry_ipAddr);//("192.168.1.203"); //Always Verify the Server IP (change every time that i connect my PC on the widora.
		 server_2.sin_family = AF_INET;
		 server_2.sin_port = htons(serv2send_scndry_port); //was htons //Carefull with firewall restrictions and, if necessary, create an exception to allow this port and the PDC program

		 //Connect to remote server
		 if (connect(status_socket_PDC_B , (struct sockaddr *)&server_2 , sizeof(server_2)) < 0)
		 {
			 puts("Socket connect ERROR with PDC 2 - c_37118 program");
			 syslog(LOG_ERR, "Error on connecting a socket B to PDC.");
			 return 1;
		 }
	 }

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 								PMU to C37118 FIFO interface open										//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Directory verificatiion - Avoid fail when fifo already exists
	if (mkdir(C_FIFO_PATH_SERVER,777) == -1)
	{
		remove(C_FIFO_PATH_SERVER);
	}
	mkdir(C_FIFO_PATH_SERVER,777); //always re-create the file

	if (access(C_FIFO_PATHNAME_SERVER, F_OK) == -1)
	{
		res = mkfifo(C_FIFO_PATHNAME_SERVER, 0777);
		if (res != 0)
		{
			fprintf(stderr, "C37118 :Could not create fifo to pmu program %s\n", C_FIFO_PATHNAME_SERVER);
			syslog(LOG_ERR, "Error on creating fifo from pmu_synchrophasor program. Return: %d", res);
			exit(EXIT_FAILURE);
		}
	}
	puts("oppening fifo - wait pmu program");
	pmu_pipe_fd = open(C_FIFO_PATHNAME_SERVER, open_mode);
	if (pmu_pipe_fd == -1)
	{
		printf("C37118 :failed on open FIFO to pmu program");
		syslog(LOG_ERR, "Error on openning fifo from pmu_synchrophasor program. Return: %d", pmu_pipe_fd);
		exit(EXIT_FAILURE);
	}
    puts("Starting C37118 program.");

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 											Infinite Loop												//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	while(1)
	{
		//Read buffer from pmu program FIFO
		res = read(pmu_pipe_fd, pmu_buffer, fifo_size_in_bytes);
		if (res == -1)
		{
			printf("PROBLEM DETECTED");
			syslog(LOG_ERR, "Error on reading data from pmu_synchrophasor. Return: %d", res);

		}

		frame_word[0] = C_LEADING_BYTE;
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 							Verify frame type and mount the data packet									//
		//																										//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		if(pmu_buffer[1] == C_CFG_2) //cfg-2 frame configuration 2
		{
			//puts("top Configuration frame 2");
			if (number_of_phasors_to_send == 3) //Phasor VA, VB, VC
			{
				//puts("Send C_CFG2 frame.");
				frame_word[1] = (C_CFG_2 <<4) + C_PMU_FRAME_VERSION; //fr_index = 1
				frame_word[4] = IDCODE_pointer[1];//(read_data.IDCODE>>8); //idcodeMSB
				frame_word[5] = IDCODE_pointer[0];//(read_data.IDCODE); //IDCODE LSB
				frame_word[6] = pmu_buffer[2];//read_data.soc_val>>24; //SOC MSBMSB
				frame_word[7] = pmu_buffer[3];//read_data.soc_val>>16; //SOC MSBLSB
				frame_word[8] = pmu_buffer[4];//read_data.soc_val>>8; //SOC LSBMSB
				frame_word[9] = pmu_buffer[5];//read_data.soc_val; //SOC LSBLSB
				frame_word[10] = pmu_buffer[6];//read_data.fracsec>>24; //fracsec MSBMSB
				frame_word[11] = pmu_buffer[7];//read_data.fracsec>>16; //fracsec MSBLSB
				frame_word[12] = pmu_buffer[8];//read_data.fracsec>>8; //fracsec LSBMSB
				frame_word[13] = pmu_buffer[9];//read_data.fracsec; //fracsec LSBLSB
				//timebase
				frame_word[14] = pmu_buffer[10];//read_data.TIME_BASE>>24; //TIME_BASE MSBMSB
				frame_word[15] = pmu_buffer[11];//read_data.TIME_BASE>>16; //TIME_BASE MSBLSB
				frame_word[16] = pmu_buffer[12];//read_data.TIME_BASE>>8; //TIME_BASE LSBMSB
				frame_word[17] = pmu_buffer[13];//read_data.TIME_BASE; //TIME_BASE LSBLSB
				frame_word[18] = 0; //num_pmus
				frame_word[19] = 1; //num_pmus
				//Name PMU
				frame_word[20] = (uint8_t)dpmu_name.name[0]; //name
				frame_word[21] = (uint8_t)dpmu_name.name[1]; //name
				frame_word[22] = (uint8_t)dpmu_name.name[2]; //name
				frame_word[23] = (uint8_t)dpmu_name.name[3]; //name
				frame_word[24] = (uint8_t)dpmu_name.name[4]; // name
				frame_word[25] = (uint8_t)dpmu_name.name[5]; //name
				frame_word[26] = (uint8_t)dpmu_name.name[6]; // name
				frame_word[27] = (uint8_t)dpmu_name.name[7]; // name
				frame_word[28] = (uint8_t)dpmu_name.name[8]; // name
				frame_word[29] = (uint8_t)dpmu_name.name[9]; // name
				frame_word[30] = (uint8_t)dpmu_name.name[10]; // name
				frame_word[31] = (uint8_t)dpmu_name.name[11]; //name
				frame_word[32] = (uint8_t)dpmu_name.name[12]; // name
				frame_word[33] = (uint8_t)dpmu_name.name[13]; //name
				frame_word[34] = (uint8_t)dpmu_name.name[14]; //name
				frame_word[35] = (uint8_t)dpmu_name.name[15]; //name
				//IDCODE2 //same because the single pmu
				frame_word[36] = IDCODE_pointer[1];//read_data.IDCODE>>8; //IDCODE2
				frame_word[37] = IDCODE_pointer[0];//read_data.IDCODE; //IDCODE2
				//Format field
				frame_word[38] = (uint8_t)FORMAT_MSB; //format of phasor and frequency fields
				frame_word[39] = (uint8_t)FORMAT_LSB; //
				//Phasors number
				frame_word[40] = (uint8_t)number_of_phasors_to_send>>8; //PHASORS AMOUNT
				frame_word[41] = (uint8_t)number_of_phasors_to_send; //
				//Analog amount
				frame_word[42] = (uint8_t)ANNMR_MSB; //PHASORS AMOUNT
				frame_word[43] = (uint8_t)ANNMR_LSB;   //
				//DIGITAL STATUS
				frame_word[44] = (uint8_t)DGNMR_MSB; //Digital status word
				frame_word[45] = (uint8_t)DGNMR_LSB;   //
				//PhasorA
				frame_word[46] = (uint8_t)C_CHAR_P;
				frame_word[47] = (uint8_t)C_CHAR_h;
				frame_word[48] = (uint8_t)C_CHAR_a;
				frame_word[49] = (uint8_t)C_CHAR_s;
				frame_word[50] = (uint8_t)C_CHAR_o;
				frame_word[51] = (uint8_t)C_CHAR_r;
				frame_word[52] = (uint8_t)C_CHAR__;
				frame_word[53] = (uint8_t)C_CHAR_A;
				frame_word[54] = (uint8_t)C_CHAR_SPACE;
				frame_word[55] = (uint8_t)C_CHAR_SPACE;
				frame_word[56] = (uint8_t)C_CHAR_SPACE;
				frame_word[57] = (uint8_t)C_CHAR_SPACE;
				frame_word[58] = (uint8_t)C_CHAR_SPACE;
				frame_word[59] = (uint8_t)C_CHAR_SPACE;
				frame_word[60] = (uint8_t)C_CHAR_SPACE;
				frame_word[61] = (uint8_t)C_CHAR_SPACE;
				//PhasorB
				frame_word[62] = (uint8_t)C_CHAR_P;
				frame_word[63] = (uint8_t)C_CHAR_h;
				frame_word[64] = (uint8_t)C_CHAR_a;
				frame_word[65] = (uint8_t)C_CHAR_s;
				frame_word[66] = (uint8_t)C_CHAR_o;
				frame_word[67] = (uint8_t)C_CHAR_r;
				frame_word[68] = (uint8_t)C_CHAR__;
				frame_word[69] = (uint8_t)C_CHAR_B;
				frame_word[70] = (uint8_t)C_CHAR_SPACE;
				frame_word[71] = (uint8_t)C_CHAR_SPACE;
				frame_word[72] = (uint8_t)C_CHAR_SPACE;
				frame_word[73] = (uint8_t)C_CHAR_SPACE;
				frame_word[74] = (uint8_t)C_CHAR_SPACE;
				frame_word[75] = (uint8_t)C_CHAR_SPACE;
				frame_word[76] = (uint8_t)C_CHAR_SPACE;
				frame_word[77] = (uint8_t)C_CHAR_SPACE;
				//PhasorC
				frame_word[78] = (uint8_t)C_CHAR_P;
				frame_word[79] = (uint8_t)C_CHAR_h;
				frame_word[80] = (uint8_t)C_CHAR_a;
				frame_word[81] = (uint8_t)C_CHAR_s;
				frame_word[82] = (uint8_t)C_CHAR_o;
				frame_word[83] = (uint8_t)C_CHAR_r;
				frame_word[84] = (uint8_t)C_CHAR__;
				frame_word[85] = (uint8_t)C_CHAR_C;
				frame_word[86] = (uint8_t)C_CHAR_SPACE;
				frame_word[87] = (uint8_t)C_CHAR_SPACE;
				frame_word[88] = (uint8_t)C_CHAR_SPACE;
				frame_word[89] = (uint8_t)C_CHAR_SPACE;
				frame_word[90] = (uint8_t)C_CHAR_SPACE;
				frame_word[91] = (uint8_t)C_CHAR_SPACE;
				frame_word[92] = (uint8_t)C_CHAR_SPACE;
				frame_word[93] = (uint8_t)C_CHAR_SPACE;
				//PHUNIT -- for phasor A
				frame_word[94] = (uint8_t)(PHUNIT_POSS_BYT4);
				frame_word[95] = (uint8_t)PHUNIT_POSS_BYT3;
				frame_word[96] = (uint8_t)PHUNIT_POSS_BYT2;
				frame_word[97] = (uint8_t)PHUNIT_POSS_BYT1;
				//PHUNIT -- for phasor B
				frame_word[98] = (uint8_t)(PHUNIT_POSS_BYT4);
				frame_word[99] = (uint8_t)PHUNIT_POSS_BYT3;
				frame_word[100] = (uint8_t)PHUNIT_POSS_BYT2;
				frame_word[101] = (uint8_t)PHUNIT_POSS_BYT1;
				//PHUNIT -- for phasor C
				frame_word[102] = (uint8_t)(PHUNIT_POSS_BYT4);
				frame_word[103] = (uint8_t)PHUNIT_POSS_BYT3;
				frame_word[104] = (uint8_t)PHUNIT_POSS_BYT2;
				frame_word[105] = (uint8_t)PHUNIT_POSS_BYT1;
				//FNOM
				frame_word[106] = (uint8_t)(_nominal_freq_flag>>8);
				frame_word[107] = (uint8_t)_nominal_freq_flag;
				//CFGCNT
				frame_word[108] = (uint8_t)(C_CFGCNT>>8);
				frame_word[109] = (uint8_t)C_CFGCNT;
				//No repeat of 9-27 fields
				//DATA_RATE
				frame_word[110] = (uint8_t)(_nominal_freq>>8);
				frame_word[111] = (uint8_t)_nominal_freq;
				// Neeeding an CRC 16
				fr_index = 111+1+2; //(+1 of byte [0] and +2 of CRC)
			}
			else if (number_of_phasors_to_send == 6) //Phasors VA, IA, VB, IB, VC, IC
			{
				frame_word[1] = (C_CFG_2 <<4) + C_PMU_FRAME_VERSION; //fr_index = 1
				frame_word[4] = IDCODE_pointer[1];//(read_data.IDCODE>>8); //idcodeMSB
				frame_word[5] = IDCODE_pointer[0];//(read_data.IDCODE); //IDCODE LSB
				frame_word[6] = pmu_buffer[2];//read_data.soc_val>>24; //SOC MSBMSB
				frame_word[7] = pmu_buffer[3];//read_data.soc_val>>16; //SOC MSBLSB
				frame_word[8] = pmu_buffer[4];//read_data.soc_val>>8; //SOC LSBMSB
				frame_word[9] = pmu_buffer[5];//read_data.soc_val; //SOC LSBLSB
				frame_word[10] = pmu_buffer[6];//read_data.fracsec>>24; //fracsec MSBMSB
				frame_word[11] = pmu_buffer[7];//read_data.fracsec>>16; //fracsec MSBLSB
				frame_word[12] = pmu_buffer[8];//read_data.fracsec>>8; //fracsec LSBMSB
				frame_word[13] = pmu_buffer[9];//read_data.fracsec; //fracsec LSBLSB
				//timebase
				frame_word[14] = pmu_buffer[10];//read_data.TIME_BASE>>24; //TIME_BASE MSBMSB
				frame_word[15] = pmu_buffer[11];//read_data.TIME_BASE>>16; //TIME_BASE MSBLSB
				frame_word[16] = pmu_buffer[12];//read_data.TIME_BASE>>8; //TIME_BASE LSBMSB
				frame_word[17] = pmu_buffer[13];//read_data.TIME_BASE; //TIME_BASE LSBLSB
				//Num of PMUs
				frame_word[18] = 0; //num_pmus
				frame_word[19] = 1; //num_pmus
				//Name PMU
				frame_word[20] = (uint8_t)dpmu_name.name[0]; //name
				frame_word[21] = (uint8_t)dpmu_name.name[1]; //name
				frame_word[22] = (uint8_t)dpmu_name.name[2]; //name
				frame_word[23] = (uint8_t)dpmu_name.name[3]; //name
				frame_word[24] = (uint8_t)dpmu_name.name[4]; // name
				frame_word[25] = (uint8_t)dpmu_name.name[5]; //name
				frame_word[26] = (uint8_t)dpmu_name.name[6]; // name
				frame_word[27] = (uint8_t)dpmu_name.name[7]; // name
				frame_word[28] = (uint8_t)dpmu_name.name[8]; // name
				frame_word[29] = (uint8_t)dpmu_name.name[9]; // name
				frame_word[30] = (uint8_t)dpmu_name.name[10]; // name
				frame_word[31] = (uint8_t)dpmu_name.name[11]; //name
				frame_word[32] = (uint8_t)dpmu_name.name[12]; // name
				frame_word[33] = (uint8_t)dpmu_name.name[13]; //name
				frame_word[34] = (uint8_t)dpmu_name.name[14]; //name
				frame_word[35] = (uint8_t)dpmu_name.name[15]; //name
				//IDCODE2 //same because the single pmu
				frame_word[36] = IDCODE_pointer[1];//read_data.IDCODE>>8; //IDCODE2
				frame_word[37] = IDCODE_pointer[0];//read_data.IDCODE; //IDCODE2
				//Format field
				frame_word[38] = (uint8_t)FORMAT_MSB; //format of phasor and frequency fields
				frame_word[39] = (uint8_t)FORMAT_LSB; //
				//Phasors number
				frame_word[40] = (uint8_t)number_of_phasors_to_send>>8; //PHASORS AMOUNT
				frame_word[41] = (uint8_t)number_of_phasors_to_send; //
				//Analog amount
				frame_word[42] = (uint8_t)ANNMR_MSB; //PHASORS AMOUNT
				frame_word[43] = (uint8_t)ANNMR_LSB;   //
				//DIGITAL STATUS
				frame_word[44] = (uint8_t)DGNMR_MSB; //Digital status word
				frame_word[45] = (uint8_t)DGNMR_LSB;   //
				//PhasorA
				frame_word[46] = (uint8_t)C_CHAR_P;
				frame_word[47] = (uint8_t)C_CHAR_h;
				frame_word[48] = (uint8_t)C_CHAR_a;
				frame_word[49] = (uint8_t)C_CHAR_s;
				frame_word[50] = (uint8_t)C_CHAR_o;
				frame_word[51] = (uint8_t)C_CHAR_r;
				frame_word[52] = (uint8_t)C_CHAR__;
				frame_word[53] = (uint8_t)C_CHAR_A;
				frame_word[54] = (uint8_t)C_CHAR_SPACE;
				frame_word[55] = (uint8_t)C_CHAR_SPACE;
				frame_word[56] = (uint8_t)C_CHAR_SPACE;
				frame_word[57] = (uint8_t)C_CHAR_SPACE;
				frame_word[58] = (uint8_t)C_CHAR_SPACE;
				frame_word[59] = (uint8_t)C_CHAR_SPACE;
				frame_word[60] = (uint8_t)C_CHAR_SPACE;
				frame_word[61] = (uint8_t)C_CHAR_SPACE;
				//PhasorA (Current)
				frame_word[62] = (uint8_t)C_CHAR_P;
				frame_word[63] = (uint8_t)C_CHAR_h;
				frame_word[64] = (uint8_t)C_CHAR_a;
				frame_word[65] = (uint8_t)C_CHAR_s;
				frame_word[66] = (uint8_t)C_CHAR_o;
				frame_word[67] = (uint8_t)C_CHAR_r;
				frame_word[68] = (uint8_t)C_CHAR__;
				frame_word[69] = (uint8_t)C_CHAR_A;
				frame_word[70] = (uint8_t)C_CHAR_SPACE;
				frame_word[71] = (uint8_t)C_CHAR_SPACE;
				frame_word[72] = (uint8_t)C_CHAR_SPACE;
				frame_word[73] = (uint8_t)C_CHAR_SPACE;
				frame_word[74] = (uint8_t)C_CHAR_SPACE;
				frame_word[75] = (uint8_t)C_CHAR_SPACE;
				frame_word[76] = (uint8_t)C_CHAR_SPACE;
				frame_word[77] = (uint8_t)C_CHAR_SPACE;
				//PhasorB (Voltage)
				frame_word[78] = (uint8_t)C_CHAR_P;
				frame_word[79] = (uint8_t)C_CHAR_h;
				frame_word[80] = (uint8_t)C_CHAR_a;
				frame_word[81] = (uint8_t)C_CHAR_s;
				frame_word[82] = (uint8_t)C_CHAR_o;
				frame_word[83] = (uint8_t)C_CHAR_r;
				frame_word[84] = (uint8_t)C_CHAR__;
				frame_word[85] = (uint8_t)C_CHAR_B;
				frame_word[86] = (uint8_t)C_CHAR_SPACE;
				frame_word[87] = (uint8_t)C_CHAR_SPACE;
				frame_word[88] = (uint8_t)C_CHAR_SPACE;
				frame_word[89] = (uint8_t)C_CHAR_SPACE;
				frame_word[90] = (uint8_t)C_CHAR_SPACE;
				frame_word[91] = (uint8_t)C_CHAR_SPACE;
				frame_word[92] = (uint8_t)C_CHAR_SPACE;
				frame_word[93] = (uint8_t)C_CHAR_SPACE;
				//PhasorB (Current)
				frame_word[94] = (uint8_t)C_CHAR_P;
				frame_word[95] = (uint8_t)C_CHAR_h;
				frame_word[96] = (uint8_t)C_CHAR_a;
				frame_word[97] = (uint8_t)C_CHAR_s;
				frame_word[98] = (uint8_t)C_CHAR_o;
				frame_word[99] = (uint8_t)C_CHAR_r;
				frame_word[100] = (uint8_t)C_CHAR__;
				frame_word[101] = (uint8_t)C_CHAR_B;
				frame_word[102] = (uint8_t)C_CHAR_SPACE;
				frame_word[103] = (uint8_t)C_CHAR_SPACE;
				frame_word[104] = (uint8_t)C_CHAR_SPACE;
				frame_word[105] = (uint8_t)C_CHAR_SPACE;
				frame_word[106] = (uint8_t)C_CHAR_SPACE;
				frame_word[107] = (uint8_t)C_CHAR_SPACE;
				frame_word[108] = (uint8_t)C_CHAR_SPACE;
				frame_word[109] = (uint8_t)C_CHAR_SPACE;
				//PhasorC (Voltage)
				frame_word[110] = (uint8_t)C_CHAR_P;
				frame_word[111] = (uint8_t)C_CHAR_h;
				frame_word[112] = (uint8_t)C_CHAR_a;
				frame_word[113] = (uint8_t)C_CHAR_s;
				frame_word[114] = (uint8_t)C_CHAR_o;
				frame_word[115] = (uint8_t)C_CHAR_r;
				frame_word[116] = (uint8_t)C_CHAR__;
				frame_word[117] = (uint8_t)C_CHAR_C;
				frame_word[118] = (uint8_t)C_CHAR_SPACE;
				frame_word[119] = (uint8_t)C_CHAR_SPACE;
				frame_word[120] = (uint8_t)C_CHAR_SPACE;
				frame_word[121] = (uint8_t)C_CHAR_SPACE;
				frame_word[122] = (uint8_t)C_CHAR_SPACE;
				frame_word[123] = (uint8_t)C_CHAR_SPACE;
				frame_word[124] = (uint8_t)C_CHAR_SPACE;
				frame_word[125] = (uint8_t)C_CHAR_SPACE;
				//PhasorC (Current)
				frame_word[126] = (uint8_t)C_CHAR_P;
				frame_word[127] = (uint8_t)C_CHAR_h;
				frame_word[128] = (uint8_t)C_CHAR_a;
				frame_word[129] = (uint8_t)C_CHAR_s;
				frame_word[130] = (uint8_t)C_CHAR_o;
				frame_word[131] = (uint8_t)C_CHAR_r;
				frame_word[132] = (uint8_t)C_CHAR__;
				frame_word[133] = (uint8_t)C_CHAR_C;
				frame_word[134] = (uint8_t)C_CHAR_SPACE;
				frame_word[135] = (uint8_t)C_CHAR_SPACE;
				frame_word[136] = (uint8_t)C_CHAR_SPACE;
				frame_word[137] = (uint8_t)C_CHAR_SPACE;
				frame_word[138] = (uint8_t)C_CHAR_SPACE;
				frame_word[139] = (uint8_t)C_CHAR_SPACE;
				frame_word[140] = (uint8_t)C_CHAR_SPACE;
				frame_word[141] = (uint8_t)C_CHAR_SPACE;
				//PHUNIT -- for phasor A (voltage)
				frame_word[142] = (uint8_t)(PHUNIT_POSS_BYT4);
				frame_word[143] = (uint8_t)PHUNIT_POSS_BYT3;
				frame_word[144] = (uint8_t)PHUNIT_POSS_BYT2;
				frame_word[145] = (uint8_t)PHUNIT_POSS_BYT1;
				//PHUNIT -- for phasor A (Current)
				frame_word[146] = (uint8_t)(PHUNIT_CURRNT_BYT4);
				frame_word[147] = (uint8_t)PHUNIT_CURRNT_BYT3;
				frame_word[148] = (uint8_t)PHUNIT_CURRNT_BYT2;
				frame_word[149] = (uint8_t)PHUNIT_CURRNT_BYT1;
				//PHUNIT -- for phasor B (Voltage)
				frame_word[150] = (uint8_t)(PHUNIT_POSS_BYT4);
				frame_word[151] = (uint8_t)PHUNIT_POSS_BYT3;
				frame_word[152] = (uint8_t)PHUNIT_POSS_BYT2;
				frame_word[153] = (uint8_t)PHUNIT_POSS_BYT1;
				//PHUNIT -- for phasor B (Current)
				frame_word[154] = (uint8_t)(PHUNIT_CURRNT_BYT4);
				frame_word[155] = (uint8_t)PHUNIT_CURRNT_BYT3;
				frame_word[156] = (uint8_t)PHUNIT_CURRNT_BYT2;
				frame_word[157] = (uint8_t)PHUNIT_CURRNT_BYT1;
				//PHUNIT -- for phasor C (Voltage)
				frame_word[158] = (uint8_t)(PHUNIT_POSS_BYT4);
				frame_word[159] = (uint8_t)PHUNIT_POSS_BYT3;
				frame_word[160] = (uint8_t)PHUNIT_POSS_BYT2;
				frame_word[161] = (uint8_t)PHUNIT_POSS_BYT1;
				//PHUNIT -- for phasor C (Current)
				frame_word[162] = (uint8_t)(PHUNIT_CURRNT_BYT4);
				frame_word[163] = (uint8_t)PHUNIT_CURRNT_BYT3;
				frame_word[164] = (uint8_t)PHUNIT_CURRNT_BYT2;
				frame_word[165] = (uint8_t)PHUNIT_CURRNT_BYT1;
				//FNOM
				frame_word[166] = (uint8_t)(_nominal_freq_flag>>8);
				frame_word[167] = (uint8_t)_nominal_freq_flag;
				//CFGCNT
				frame_word[168] = (uint8_t)(C_CFGCNT>>8);
				frame_word[169] = (uint8_t)C_CFGCNT;
				//No repeat of 9-27 fields
				//DATA_RATE
				frame_word[170] = (uint8_t)(_nominal_freq>>8);
				frame_word[171] = (uint8_t)_nominal_freq;
				// Neeeding an CRC 16
				fr_index = 171+1+2; //(+1 of byte [0] and +2 of CRC)
			}
			else if (number_of_phasors_to_send == 7) //Phasors VA, IA, VB, IB, VC, IC, PosSeqV
			{
				frame_word[1] = (C_CFG_2 <<4) + C_PMU_FRAME_VERSION; //fr_index = 1
				frame_word[4] = IDCODE_pointer[1];//(read_data.IDCODE>>8); //idcodeMSB
				frame_word[5] = IDCODE_pointer[0];//(read_data.IDCODE); //IDCODE LSB
				frame_word[6] = pmu_buffer[2];//read_data.soc_val>>24; //SOC MSBMSB
				frame_word[7] = pmu_buffer[3];//read_data.soc_val>>16; //SOC MSBLSB
				frame_word[8] = pmu_buffer[4];//read_data.soc_val>>8; //SOC LSBMSB
				frame_word[9] = pmu_buffer[5];//read_data.soc_val; //SOC LSBLSB
				frame_word[10] = pmu_buffer[6];//read_data.fracsec>>24; //fracsec MSBMSB
				frame_word[11] = pmu_buffer[7];//read_data.fracsec>>16; //fracsec MSBLSB
				frame_word[12] = pmu_buffer[8];//read_data.fracsec>>8; //fracsec LSBMSB
				frame_word[13] = pmu_buffer[9];//read_data.fracsec; //fracsec LSBLSB
				//timebase
				frame_word[14] = pmu_buffer[10];//read_data.TIME_BASE>>24; //TIME_BASE MSBMSB
				frame_word[15] = pmu_buffer[11];//read_data.TIME_BASE>>16; //TIME_BASE MSBLSB
				frame_word[16] = pmu_buffer[12];//read_data.TIME_BASE>>8; //TIME_BASE LSBMSB
				frame_word[17] = pmu_buffer[13];//read_data.TIME_BASE; //TIME_BASE LSBLSB
				//Num of PMUs
				frame_word[18] = 0; //num_pmus
				frame_word[19] = 1; //num_pmus
				//Name PMU
				frame_word[20] = (uint8_t)dpmu_name.name[0]; //name
				frame_word[21] = (uint8_t)dpmu_name.name[1]; //name
				frame_word[22] = (uint8_t)dpmu_name.name[2]; //name
				frame_word[23] = (uint8_t)dpmu_name.name[3]; //name
				frame_word[24] = (uint8_t)dpmu_name.name[4]; // name
				frame_word[25] = (uint8_t)dpmu_name.name[5]; //name
				frame_word[26] = (uint8_t)dpmu_name.name[6]; // name
				frame_word[27] = (uint8_t)dpmu_name.name[7]; // name
				frame_word[28] = (uint8_t)dpmu_name.name[8]; // name
				frame_word[29] = (uint8_t)dpmu_name.name[9]; // name
				frame_word[30] = (uint8_t)dpmu_name.name[10]; // name
				frame_word[31] = (uint8_t)dpmu_name.name[11]; //name
				frame_word[32] = (uint8_t)dpmu_name.name[12]; // name
				frame_word[33] = (uint8_t)dpmu_name.name[13]; //name
				frame_word[34] = (uint8_t)dpmu_name.name[14]; //name
				frame_word[35] = (uint8_t)dpmu_name.name[15]; //name
				//IDCODE2 //same because the single pmu
				frame_word[36] = IDCODE_pointer[1];//read_data.IDCODE>>8; //IDCODE2
				frame_word[37] = IDCODE_pointer[0];//read_data.IDCODE; //IDCODE2
				//Format field
				frame_word[38] = (uint8_t)FORMAT_MSB; //format of phasor and frequency fields
				frame_word[39] = (uint8_t)FORMAT_LSB; //
				//Phasors number
				frame_word[40] = (uint8_t)number_of_phasors_to_send>>8; //PHASORS AMOUNT
				frame_word[41] = (uint8_t)number_of_phasors_to_send; //
				//Analog amount
				frame_word[42] = (uint8_t)ANNMR_MSB; //PHASORS AMOUNT
				frame_word[43] = (uint8_t)ANNMR_LSB;   //
				//DIGITAL STATUS
				frame_word[44] = (uint8_t)DGNMR_MSB; //Digital status word
				frame_word[45] = (uint8_t)DGNMR_LSB;   //
				//PhasorA
				frame_word[46] = (uint8_t)C_CHAR_P;
				frame_word[47] = (uint8_t)C_CHAR_h;
				frame_word[48] = (uint8_t)C_CHAR_a;
				frame_word[49] = (uint8_t)C_CHAR_s;
				frame_word[50] = (uint8_t)C_CHAR_o;
				frame_word[51] = (uint8_t)C_CHAR_r;
				frame_word[52] = (uint8_t)C_CHAR__;
				frame_word[53] = (uint8_t)C_CHAR_A;
				frame_word[54] = (uint8_t)C_CHAR_SPACE;
				frame_word[55] = (uint8_t)C_CHAR_SPACE;
				frame_word[56] = (uint8_t)C_CHAR_SPACE;
				frame_word[57] = (uint8_t)C_CHAR_SPACE;
				frame_word[58] = (uint8_t)C_CHAR_SPACE;
				frame_word[59] = (uint8_t)C_CHAR_SPACE;
				frame_word[60] = (uint8_t)C_CHAR_SPACE;
				frame_word[61] = (uint8_t)C_CHAR_SPACE;
				//PhasorA (Current)
				frame_word[62] = (uint8_t)C_CHAR_P;
				frame_word[63] = (uint8_t)C_CHAR_h;
				frame_word[64] = (uint8_t)C_CHAR_a;
				frame_word[65] = (uint8_t)C_CHAR_s;
				frame_word[66] = (uint8_t)C_CHAR_o;
				frame_word[67] = (uint8_t)C_CHAR_r;
				frame_word[68] = (uint8_t)C_CHAR__;
				frame_word[69] = (uint8_t)C_CHAR_A;
				frame_word[70] = (uint8_t)C_CHAR_SPACE;
				frame_word[71] = (uint8_t)C_CHAR_SPACE;
				frame_word[72] = (uint8_t)C_CHAR_SPACE;
				frame_word[73] = (uint8_t)C_CHAR_SPACE;
				frame_word[74] = (uint8_t)C_CHAR_SPACE;
				frame_word[75] = (uint8_t)C_CHAR_SPACE;
				frame_word[76] = (uint8_t)C_CHAR_SPACE;
				frame_word[77] = (uint8_t)C_CHAR_SPACE;
				//PhasorB (Voltage)
				frame_word[78] = (uint8_t)C_CHAR_P;
				frame_word[79] = (uint8_t)C_CHAR_h;
				frame_word[80] = (uint8_t)C_CHAR_a;
				frame_word[81] = (uint8_t)C_CHAR_s;
				frame_word[82] = (uint8_t)C_CHAR_o;
				frame_word[83] = (uint8_t)C_CHAR_r;
				frame_word[84] = (uint8_t)C_CHAR__;
				frame_word[85] = (uint8_t)C_CHAR_B;
				frame_word[86] = (uint8_t)C_CHAR_SPACE;
				frame_word[87] = (uint8_t)C_CHAR_SPACE;
				frame_word[88] = (uint8_t)C_CHAR_SPACE;
				frame_word[89] = (uint8_t)C_CHAR_SPACE;
				frame_word[90] = (uint8_t)C_CHAR_SPACE;
				frame_word[91] = (uint8_t)C_CHAR_SPACE;
				frame_word[92] = (uint8_t)C_CHAR_SPACE;
				frame_word[93] = (uint8_t)C_CHAR_SPACE;
				//PhasorB (Current)
				frame_word[94] = (uint8_t)C_CHAR_P;
				frame_word[95] = (uint8_t)C_CHAR_h;
				frame_word[96] = (uint8_t)C_CHAR_a;
				frame_word[97] = (uint8_t)C_CHAR_s;
				frame_word[98] = (uint8_t)C_CHAR_o;
				frame_word[99] = (uint8_t)C_CHAR_r;
				frame_word[100] = (uint8_t)C_CHAR__;
				frame_word[101] = (uint8_t)C_CHAR_B;
				frame_word[102] = (uint8_t)C_CHAR_SPACE;
				frame_word[103] = (uint8_t)C_CHAR_SPACE;
				frame_word[104] = (uint8_t)C_CHAR_SPACE;
				frame_word[105] = (uint8_t)C_CHAR_SPACE;
				frame_word[106] = (uint8_t)C_CHAR_SPACE;
				frame_word[107] = (uint8_t)C_CHAR_SPACE;
				frame_word[108] = (uint8_t)C_CHAR_SPACE;
				frame_word[109] = (uint8_t)C_CHAR_SPACE;
				//PhasorC (Voltage)
				frame_word[110] = (uint8_t)C_CHAR_P;
				frame_word[111] = (uint8_t)C_CHAR_h;
				frame_word[112] = (uint8_t)C_CHAR_a;
				frame_word[113] = (uint8_t)C_CHAR_s;
				frame_word[114] = (uint8_t)C_CHAR_o;
				frame_word[115] = (uint8_t)C_CHAR_r;
				frame_word[116] = (uint8_t)C_CHAR__;
				frame_word[117] = (uint8_t)C_CHAR_C;
				frame_word[118] = (uint8_t)C_CHAR_SPACE;
				frame_word[119] = (uint8_t)C_CHAR_SPACE;
				frame_word[120] = (uint8_t)C_CHAR_SPACE;
				frame_word[121] = (uint8_t)C_CHAR_SPACE;
				frame_word[122] = (uint8_t)C_CHAR_SPACE;
				frame_word[123] = (uint8_t)C_CHAR_SPACE;
				frame_word[124] = (uint8_t)C_CHAR_SPACE;
				frame_word[125] = (uint8_t)C_CHAR_SPACE;
				//PhasorC (Current)
				frame_word[126] = (uint8_t)C_CHAR_P;
				frame_word[127] = (uint8_t)C_CHAR_h;
				frame_word[128] = (uint8_t)C_CHAR_a;
				frame_word[129] = (uint8_t)C_CHAR_s;
				frame_word[130] = (uint8_t)C_CHAR_o;
				frame_word[131] = (uint8_t)C_CHAR_r;
				frame_word[132] = (uint8_t)C_CHAR__;
				frame_word[133] = (uint8_t)C_CHAR_C;
				frame_word[134] = (uint8_t)C_CHAR_SPACE;
				frame_word[135] = (uint8_t)C_CHAR_SPACE;
				frame_word[136] = (uint8_t)C_CHAR_SPACE;
				frame_word[137] = (uint8_t)C_CHAR_SPACE;
				frame_word[138] = (uint8_t)C_CHAR_SPACE;
				frame_word[139] = (uint8_t)C_CHAR_SPACE;
				frame_word[140] = (uint8_t)C_CHAR_SPACE;
				frame_word[141] = (uint8_t)C_CHAR_SPACE;
				//PhasorPosSeq  (Voltage)
				frame_word[142] = (uint8_t)C_CHAR_P;
				frame_word[143] = (uint8_t)C_CHAR_h;
				frame_word[144] = (uint8_t)C_CHAR_a;
				frame_word[145] = (uint8_t)C_CHAR_s;
				frame_word[146] = (uint8_t)C_CHAR_o;
				frame_word[147] = (uint8_t)C_CHAR_r;
				frame_word[148] = (uint8_t)C_CHAR__;
				frame_word[149] = (uint8_t)C_CHAR_P;
				frame_word[150] = (uint8_t)C_CHAR_o;
				frame_word[151] = (uint8_t)C_CHAR_s;
				frame_word[152] = (uint8_t)C_CHAR__;
				frame_word[153] = (uint8_t)C_CHAR_S;
				frame_word[154] = (uint8_t)C_CHAR_e;
				frame_word[155] = (uint8_t)C_CHAR_q;
				frame_word[156] = (uint8_t)C_CHAR_SPACE;
				frame_word[157] = (uint8_t)C_CHAR_SPACE;
				//PHUNIT -- for phasor A (voltage)
				frame_word[158] = (uint8_t)(PHUNIT_POSS_BYT4);
				frame_word[159] = (uint8_t)PHUNIT_POSS_BYT3;
				frame_word[160] = (uint8_t)PHUNIT_POSS_BYT2;
				frame_word[161] = (uint8_t)PHUNIT_POSS_BYT1;
				//PHUNIT -- for phasor A (Current)
				frame_word[162] = (uint8_t)(PHUNIT_CURRNT_BYT4);
				frame_word[163] = (uint8_t)PHUNIT_CURRNT_BYT3;
				frame_word[164] = (uint8_t)PHUNIT_CURRNT_BYT2;
				frame_word[165] = (uint8_t)PHUNIT_CURRNT_BYT1;
				//PHUNIT -- for phasor B (Voltage)
				frame_word[166] = (uint8_t)(PHUNIT_POSS_BYT4);
				frame_word[167] = (uint8_t)PHUNIT_POSS_BYT3;
				frame_word[168] = (uint8_t)PHUNIT_POSS_BYT2;
				frame_word[169] = (uint8_t)PHUNIT_POSS_BYT1;
				//PHUNIT -- for phasor B (Current)
				frame_word[170] = (uint8_t)(PHUNIT_CURRNT_BYT4);
				frame_word[171] = (uint8_t)PHUNIT_CURRNT_BYT3;
				frame_word[172] = (uint8_t)PHUNIT_CURRNT_BYT2;
				frame_word[173] = (uint8_t)PHUNIT_CURRNT_BYT1;
				//PHUNIT -- for phasor C (Voltage)
				frame_word[174] = (uint8_t)(PHUNIT_POSS_BYT4);
				frame_word[175] = (uint8_t)PHUNIT_POSS_BYT3;
				frame_word[176] = (uint8_t)PHUNIT_POSS_BYT2;
				frame_word[177] = (uint8_t)PHUNIT_POSS_BYT1;
				//PHUNIT -- for phasor C (Current)
				frame_word[178] = (uint8_t)(PHUNIT_CURRNT_BYT4);
				frame_word[179] = (uint8_t)PHUNIT_CURRNT_BYT3;
				frame_word[180] = (uint8_t)PHUNIT_CURRNT_BYT2;
				frame_word[181] = (uint8_t)PHUNIT_CURRNT_BYT1;
				//PHUNIT -- for phasor Pos Seq (voltage)
				frame_word[182] = (uint8_t)(PHUNIT_POSS_BYT4);
				frame_word[183] = (uint8_t)PHUNIT_POSS_BYT3;
				frame_word[184] = (uint8_t)PHUNIT_POSS_BYT2;
				frame_word[185] = (uint8_t)PHUNIT_POSS_BYT1;
				//FNOM
				frame_word[186] = (uint8_t)(_nominal_freq_flag>>8);
				frame_word[187] = (uint8_t)_nominal_freq_flag;
				//CFGCNT
				frame_word[188] = (uint8_t)(C_CFGCNT>>8);
				frame_word[189] = (uint8_t)C_CFGCNT;
				//No repeat of 9-27 fields
				//DATA_RATE
				frame_word[190] = (uint8_t)(_nominal_freq>>8);
				frame_word[191] = (uint8_t)_nominal_freq;
				// Neeeding an CRC 16
				fr_index = 191+1+2; //(+1 of byte [0] and +2 of CRC)
			}
		}
		else if(pmu_buffer[1] == C_HDR )
		{ //HEADER frame type

			//puts("top Header frame");
			frame_word[1] = (C_HDR <<4) + C_PMU_FRAME_VERSION; //fr_index = 1 //SYNC FRAME LSB FRAME
			//IDCODE
			frame_word[4] = IDCODE_pointer[1];//(read_data.IDCODE>>8); //idcodeMSB
			frame_word[5] = IDCODE_pointer[0];//(read_data.IDCODE); //IDCODE LSB,
			//SOC
			frame_word[6] = pmu_buffer[2];//read_data.soc_val>>24; //SOC MSBMSB
			frame_word[7] = pmu_buffer[3];//read_data.soc_val>>16; //SOC MSBLSB
			frame_word[8] = pmu_buffer[4];//read_data.soc_val>>8; //SOC LSBMSB
			frame_word[9] = pmu_buffer[5];//read_data.soc_val; //SOC LSBLSB
			//FRACSEC
			frame_word[10] = pmu_buffer[6];//read_data.fracsec>>24; //fracsec MSBMSB
			frame_word[11] = pmu_buffer[7];//read_data.fracsec>>16; //fracsec MSBLSB
			frame_word[12] = pmu_buffer[8];//read_data.fracsec>>8; //fracsec LSBMSB
			frame_word[13] = pmu_buffer[9];//read_data.fracsec; //fracsec LSBLSB
			//DATA - HEADER
			for (i=0; i <= (dpmu_header_info.header_length-1); i++) //dpmu_header_info.header_length
			{
				frame_word[14+i] = (uint8_t)dpmu_header_info.header[i];
			}
			//CRC 16
			fr_index = (13+i)+1+2;//(+1 of byte [0] and +2 of CRC)

		}
		else if(pmu_buffer[1] == C_DATA_FRAME ) //DATA frame type of phasor.
		{
			if (number_of_phasors_to_send == 3) //Phasor VA, VB, VC
			{
				frame_word[1] = 0 + C_PMU_FRAME_VERSION; //fr_index = 1 //SYNC FRAME LSB FRAME
				//IDCODE
				frame_word[4] = IDCODE_pointer[1];//(read_data.IDCODE>>8); //idcodeMSB
				frame_word[5] = IDCODE_pointer[0];//(read_data.IDCODE); //IDCODE LSB
				//SOC
				frame_word[6] = pmu_buffer[2];//read_data.soc_val>>24; //SOC MSBMSB
				frame_word[7] = pmu_buffer[3];//read_data.soc_val>>16; //SOC MSBLSB
				frame_word[8] = pmu_buffer[4];//read_data.soc_val>>8; //SOC LSBMSB
				frame_word[9] = pmu_buffer[5];//read_data.soc_val; //SOC LSBLSB
				//FRACSEC
				frame_word[10] = pmu_buffer[6];//read_data.fracsec>>24; //fracsec MSBMSB
				frame_word[11] = pmu_buffer[7];//read_data.fracsec>>16; //fracsec MSBLSB
				frame_word[12] = pmu_buffer[8];//read_data.fracsec>>8; //fracsec LSBMSB
				frame_word[13] = pmu_buffer[9];//read_data.fracsec; //fracsec LSBLSB
				//Status reg
				frame_word[14] = pmu_buffer[10];//read_data.STAT>>8; //Status MSB
				frame_word[15] = pmu_buffer[11];//read_data.STAT; //Status LSB
				//Phasor A
				frame_word[16] = pmu_buffer[12];//read_data.phsrAreal_val>>8; //real part msb
				frame_word[17] = pmu_buffer[13];//read_data.phsrAreal_val; //real lsb
				frame_word[18] = pmu_buffer[14];
				frame_word[19] = pmu_buffer[15];

				frame_word[20] = pmu_buffer[16];//read_data.phsrAimagin_val>>8; //imag MSB
				frame_word[21] = pmu_buffer[17];//read_data.phsrAimagin_val; //imag LSB
				frame_word[22] = pmu_buffer[18];
				frame_word[23] = pmu_buffer[19];
				//Phasor B
				frame_word[24] = pmu_buffer[20];//read_data.phsrBreal_val>>8; //real part msb
				frame_word[25] = pmu_buffer[21];//read_data.phsrBreal_val; //real lsb
				frame_word[26] = pmu_buffer[22];
				frame_word[27] = pmu_buffer[23];

				frame_word[28] = pmu_buffer[24];//read_data.phsrBimagin_val>>8; //imag MSB
				frame_word[29] = pmu_buffer[25];//read_data.phsrBimagin_val; //imag LSB
				frame_word[30] = pmu_buffer[26];
				frame_word[31] = pmu_buffer[27];
				//Phasor C
				frame_word[32] = pmu_buffer[28];//read_data.phsrCreal_val>>8; //real part msb
				frame_word[33] = pmu_buffer[29];//read_data.phsrCreal_val; //real lsb
				frame_word[34] = pmu_buffer[30];
				frame_word[35] = pmu_buffer[31];

				frame_word[36] = pmu_buffer[32];//read_data.phsrCimagin_val>>8; //imag MSB
				frame_word[37] = pmu_buffer[33];//read_data.phsrCimagin_val; //imag LSB
				frame_word[38] = pmu_buffer[34];
				frame_word[39] = pmu_buffer[35];
				//Freq
				frame_word[40] = (pmu_buffer[36]);//((uint32_t)read_data.frequency>>24); //freq MSBMSB
				frame_word[41] = (pmu_buffer[37]);//((uint32_t)read_data.frequency>>16); //frequency MSBLSB
				frame_word[42] = (pmu_buffer[38]);//((uint32_t)read_data.frequency>>8); //freq LSBMSB
				frame_word[43] = (pmu_buffer[39]);//(uint32_t)(read_data.frequency); //frequency LSBLSB
				//Dfreq
				frame_word[44] = (pmu_buffer[40]);//((uint32_t)read_data.dfreq>>24); //freq MSBMSB
				frame_word[45] = (pmu_buffer[41]);//((uint32_t)read_data.dfreq>>16); //frequency MSBLSB
				frame_word[46] = (pmu_buffer[42]);//((uint32_t)read_data.dfreq>>8); //freq LSBMSB
				frame_word[47] = (pmu_buffer[43]);//((uint32_t)read_data.dfreq); //frequency LSB

				 fr_index = 47+1+2; //(+1 of byte [0] and +2 of CRC)
			}
			else if (number_of_phasors_to_send == 6)  //Phasors VA, IA, VB, IB, VC, IC.
			{
				frame_word[1] = 0 + C_PMU_FRAME_VERSION; //fr_index = 1 //SYNC FRAME LSB FRAME
				//IDCODE
				frame_word[4] = IDCODE_pointer[1];//(read_data.IDCODE>>8); //idcodeMSB
				frame_word[5] = IDCODE_pointer[0];//(read_data.IDCODE); //IDCODE LSB
				//SOC
				frame_word[6] = pmu_buffer[2];//read_data.soc_val>>24; //SOC MSBMSB
				frame_word[7] = pmu_buffer[3];//read_data.soc_val>>16; //SOC MSBLSB
				frame_word[8] = pmu_buffer[4];//read_data.soc_val>>8; //SOC LSBMSB
				frame_word[9] = pmu_buffer[5];//read_data.soc_val; //SOC LSBLSB
				//FRACSEC
				frame_word[10] = pmu_buffer[6];//read_data.fracsec>>24; //fracsec MSBMSB
				frame_word[11] = pmu_buffer[7];//read_data.fracsec>>16; //fracsec MSBLSB
				frame_word[12] = pmu_buffer[8];//read_data.fracsec>>8; //fracsec LSBMSB
				frame_word[13] = pmu_buffer[9];//read_data.fracsec; //fracsec LSBLSB
				//Status reg
				frame_word[14] = pmu_buffer[10];//read_data.STAT>>8; //Status MSB
				frame_word[15] = pmu_buffer[11];//read_data.STAT; //Status LSB
				//Phasor A (Voltage)
				frame_word[16] = pmu_buffer[12];//read_data.phsrAreal_val>>8; //real part msb
				frame_word[17] = pmu_buffer[13];//read_data.phsrAreal_val; //real lsb
				frame_word[18] = pmu_buffer[14];
				frame_word[19] = pmu_buffer[15];

				frame_word[20] = pmu_buffer[16];//read_data.phsrAimagin_val>>8; //imag MSB
				frame_word[21] = pmu_buffer[17];//read_data.phsrAimagin_val; //imag LSB
				frame_word[22] = pmu_buffer[18];
				frame_word[23] = pmu_buffer[19];
				//Phasor A (Current)
				frame_word[24] = 0x00;
				frame_word[25] = 0x00;
				frame_word[26] = 0x00;
				frame_word[27] = 0x00;

				frame_word[28] = 0x00;
				frame_word[29] = 0x00;
				frame_word[30] = 0x00;
				frame_word[31] = 0x00;
				//Phasor B(Voltage)
				frame_word[32] = pmu_buffer[20];//read_data.phsrBreal_val>>8; //real part msb
				frame_word[33] = pmu_buffer[21];//read_data.phsrBreal_val; //real lsb
				frame_word[34] = pmu_buffer[22];
				frame_word[35] = pmu_buffer[23];

				frame_word[36] = pmu_buffer[24];//read_data.phsrBimagin_val>>8; //imag MSB
				frame_word[37] = pmu_buffer[25];//read_data.phsrBimagin_val; //imag LSB
				frame_word[38] = pmu_buffer[26];
				frame_word[39] = pmu_buffer[27];
				//Phasor B(Current)
				frame_word[40] = 0x00;
				frame_word[41] = 0x00;
				frame_word[42] = 0x00;
				frame_word[43] = 0x00;

				frame_word[44] = 0x00;
				frame_word[45] = 0x00;
				frame_word[46] = 0x00;
				frame_word[47] = 0x00;
				//Phasor C (Voltage)
				frame_word[48] = pmu_buffer[28];//read_data.phsrCreal_val>>8; //real part msb
				frame_word[49] = pmu_buffer[29];//read_data.phsrCreal_val; //real lsb
				frame_word[50] = pmu_buffer[30];
				frame_word[51] = pmu_buffer[31];

				frame_word[52] = pmu_buffer[32];//read_data.phsrCimagin_val>>8; //imag MSB
				frame_word[53] = pmu_buffer[33];//read_data.phsrCimagin_val; //imag LSB
				frame_word[54] = pmu_buffer[34];
				frame_word[55] = pmu_buffer[35];
				//Phasor C (Current)
				frame_word[56] = 0x00;
				frame_word[57] = 0x00;
				frame_word[58] = 0x00;
				frame_word[59] = 0x00;

				frame_word[60] = 0x00;
				frame_word[61] = 0x00;
				frame_word[62] = 0x00;
				frame_word[63] = 0x00;
				//Freq
				frame_word[64] = (pmu_buffer[36]);//((uint32_t)read_data.frequency>>24); //freq MSBMSB
				frame_word[65] = (pmu_buffer[37]);//((uint32_t)read_data.frequency>>16); //frequency MSBLSB
				frame_word[66] = (pmu_buffer[38]);//((uint32_t)read_data.frequency>>8); //freq LSBMSB
				frame_word[67] = (pmu_buffer[39]);//(uint32_t)(read_data.frequency); //frequency LSBLSB
				//Dfreq
				frame_word[68] = (pmu_buffer[40]);//((uint32_t)read_data.dfreq>>24); //freq MSBMSB
				frame_word[69] = (pmu_buffer[41]);//((uint32_t)read_data.dfreq>>16); //frequency MSBLSB
				frame_word[70] = (pmu_buffer[42]);//((uint32_t)read_data.dfreq>>8); //freq LSBMSB
				frame_word[71] = (pmu_buffer[43]);//((uint32_t)read_data.dfreq); //frequency LSB

				 fr_index = 71+1+2; //(+1 of byte [0] and +2 of CRC)
			}
			else if (number_of_phasors_to_send == 7)  //Phasors VA, IA, VB, IB, VC, IC, PosSeqV
			{
				frame_word[1] = 0 + C_PMU_FRAME_VERSION; //fr_index = 1 //SYNC FRAME LSB FRAME
				//IDCODE
				frame_word[4] = IDCODE_pointer[1];//(read_data.IDCODE>>8); //idcodeMSB
				frame_word[5] = IDCODE_pointer[0];//(read_data.IDCODE); //IDCODE LSB
				//SOC
				frame_word[6] = pmu_buffer[2];//read_data.soc_val>>24; //SOC MSBMSB
				frame_word[7] = pmu_buffer[3];//read_data.soc_val>>16; //SOC MSBLSB
				frame_word[8] = pmu_buffer[4];//read_data.soc_val>>8; //SOC LSBMSB
				frame_word[9] = pmu_buffer[5];//read_data.soc_val; //SOC LSBLSB
				//FRACSEC
				frame_word[10] = pmu_buffer[6];//read_data.fracsec>>24; //fracsec MSBMSB
				frame_word[11] = pmu_buffer[7];//read_data.fracsec>>16; //fracsec MSBLSB
				frame_word[12] = pmu_buffer[8];//read_data.fracsec>>8; //fracsec LSBMSB
				frame_word[13] = pmu_buffer[9];//read_data.fracsec; //fracsec LSBLSB
				//Status reg
				frame_word[14] = pmu_buffer[10];//read_data.STAT>>8; //Status MSB
				frame_word[15] = pmu_buffer[11];//read_data.STAT; //Status LSB
				//Phasor A (Voltage)
				frame_word[16] = pmu_buffer[12];//read_data.phsrAreal_val>>8; //real part msb
				frame_word[17] = pmu_buffer[13];//read_data.phsrAreal_val; //real lsb
				frame_word[18] = pmu_buffer[14];
				frame_word[19] = pmu_buffer[15];

				frame_word[20] = pmu_buffer[16];//read_data.phsrAimagin_val>>8; //imag MSB
				frame_word[21] = pmu_buffer[17];//read_data.phsrAimagin_val; //imag LSB
				frame_word[22] = pmu_buffer[18];
				frame_word[23] = pmu_buffer[19];
				//Phasor A (Current)
				frame_word[24] = 0x00;
				frame_word[25] = 0x00;
				frame_word[26] = 0x00;
				frame_word[27] = 0x00;

				frame_word[28] = 0x00;
				frame_word[29] = 0x00;
				frame_word[30] = 0x00;
				frame_word[31] = 0x00;
				//Phasor B(Voltage)
				frame_word[32] = pmu_buffer[20];//read_data.phsrBreal_val>>8; //real part msb
				frame_word[33] = pmu_buffer[21];//read_data.phsrBreal_val; //real lsb
				frame_word[34] = pmu_buffer[22];
				frame_word[35] = pmu_buffer[23];

				frame_word[36] = pmu_buffer[24];//read_data.phsrBimagin_val>>8; //imag MSB
				frame_word[37] = pmu_buffer[25];//read_data.phsrBimagin_val; //imag LSB
				frame_word[38] = pmu_buffer[26];
				frame_word[39] = pmu_buffer[27];
				//Phasor B(Current)
				frame_word[40] = 0x00;
				frame_word[41] = 0x00;
				frame_word[42] = 0x00;
				frame_word[43] = 0x00;

				frame_word[44] = 0x00;
				frame_word[45] = 0x00;
				frame_word[46] = 0x00;
				frame_word[47] = 0x00;
				//Phasor C (Voltage)
				frame_word[48] = pmu_buffer[28];//read_data.phsrCreal_val>>8; //real part msb
				frame_word[49] = pmu_buffer[29];//read_data.phsrCreal_val; //real lsb
				frame_word[50] = pmu_buffer[30];
				frame_word[51] = pmu_buffer[31];

				frame_word[52] = pmu_buffer[32];//read_data.phsrCimagin_val>>8; //imag MSB
				frame_word[53] = pmu_buffer[33];//read_data.phsrCimagin_val; //imag LSB
				frame_word[54] = pmu_buffer[34];
				frame_word[55] = pmu_buffer[35];
				//Phasor C (Current)
				frame_word[56] = 0x00;
				frame_word[57] = 0x00;
				frame_word[58] = 0x00;
				frame_word[59] = 0x00;

				frame_word[60] = 0x00;
				frame_word[61] = 0x00;
				frame_word[62] = 0x00;
				frame_word[63] = 0x00;
				//Phasor PosSeq (Voltage)
				frame_word[64] = pmu_buffer[36];//read_data.phsrP+real_val>>8; //real part msb
				frame_word[65] = pmu_buffer[37];//read_data.phsrP+real_val; //real lsb
				frame_word[66] = pmu_buffer[38];
				frame_word[67] = pmu_buffer[39];

				frame_word[68] = pmu_buffer[40];//read_data.phsrP+imagin_val>>8; //imag MSB
				frame_word[69] = pmu_buffer[41];//read_data.phsrP+imagin_val; //imag LSB
				frame_word[70] = pmu_buffer[42];
				frame_word[71] = pmu_buffer[43];
				//Freq
				frame_word[72] = (pmu_buffer[44]);//((uint32_t)read_data.frequency>>24); //freq MSBMSB
				frame_word[73] = (pmu_buffer[45]);//((uint32_t)read_data.frequency>>16); //frequency MSBLSB
				frame_word[74] = (pmu_buffer[46]);//((uint32_t)read_data.frequency>>8); //freq LSBMSB
				frame_word[75] = (pmu_buffer[47]);//(uint32_t)(read_data.frequency); //frequency LSBLSB
				//Dfreq
				frame_word[76] = (pmu_buffer[48]);//((uint32_t)read_data.dfreq>>24); //freq MSBMSB
				frame_word[77] = (pmu_buffer[49]);//((uint32_t)read_data.dfreq>>16); //frequency MSBLSB
				frame_word[78] = (pmu_buffer[50]);//((uint32_t)read_data.dfreq>>8); //freq LSBMSB
				frame_word[79] = (pmu_buffer[51]);//((uint32_t)read_data.dfreq); //frequency LSB

				 fr_index = 79+1+2; //(+1 of byte [0] and +2 of CRC)
			}

		}//Frame type (pmu_buffer[1]) == XX

		//finishing the framesize FRAMESIZE - 2 BYTES
		frame_word[2] = fr_index_pointer[1]; //(uint8_t)((fr_index+2+1)>>8);   //+1 for the zero position and +2 because CRC
		frame_word[3] = fr_index_pointer[0]; //(fr_index+2+1);   //+1 for the zero position and +2 because CRC

		crc16_obtained = compute_crc16(frame_word,(uint)fr_index-2);//fr_index+1
		frame_word[fr_index-2] = crc16_pointer[1];//(uint16_t)crc16_obtained >> 8;
		frame_word[fr_index-1] = crc16_pointer[0];//(uint16_t)crc16_obtained;

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 											Send Packets Part											//
		//																										//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Server 1 send packet
		if( send(status_socket_PDC_A , frame_word , fr_index , 0) < 0){ // send(socket_desc_UFSC , message , strlen(message) ,0)
			puts("PDC send failed for PDC A!");
			fprintf(stderr, "Error sending packet to PDC 1 , errno: %d \n", errno);
			syslog(LOG_ERR, "Error on sending packet to PDC 1. errno: %d", errno);
		}

		//Server 2 send packet
		if (send_other_PDC_flag == TRUE){
			if( send(status_socket_PDC_B , frame_word , fr_index , 0) < 0){ // send(socket_desc_UFSC , message , strlen(message) ,0)
				puts("PDC send failed for PDC B!");
				fprintf(stderr, "Error sending packet to PDC 2 , errno: %d \n", errno);
				syslog(LOG_ERR, "Error on sending packet to PDC 2. errno: %d", errno);
			}
		}
	} //while 1

}//end main
