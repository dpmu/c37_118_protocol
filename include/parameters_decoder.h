/*
 * parameters_decoder.h
 *
 *  Created on: 08 de mar de 2020
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_PARAMETERS_DECODER_H_
#define INCLUDE_PARAMETERS_DECODER_H_
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//Public defines
#define TRUE 1
#define FALSE 0

//Structures for decoded data
typedef struct
{
	char *name;
	uint8_t name_length;
} dpmu_dec_name_st;

typedef struct
{
	char *header;
	uint8_t header_length;
} dpmu_dec_header_st;

struct dpmu_file_decoded_st
{
	dpmu_dec_name_st dec_name;
	dpmu_dec_header_st dec_header;
	char *dpmu_idcode;
	char *pdc_ip;
	char *pdc_port;
	char *send_other_pdc;
	char *scnd_pdc_ip;
	char *scnd_pdc_port;
	uint8_t number_of_phasors; // 3 for  VA, VB, VC; 6 for VA, IA, VB, IB, VC, IC; 7 for VA, IA, VB, IB, VC, IC, PosSeqV
	uint16_t nominal_sys_freq;
	uint8_t communication_protocol; //0 = UDP, 1=TCP
};

//prototype of public functions
void decode_DPMU_file_parameters(void);
dpmu_dec_name_st return_dpmu_name(void);
dpmu_dec_header_st return_dpmu_header(void);
uint16_t return_dpmu_idcode(void);
char *return_dpmu_pdc_destinyport(void);
char *return_dpmu_scnd_pdc_destinyip(void);
char *return_dpmu_scnd_pdc_destinyport(void);
uint8_t return_dpmu_number_of_phasors(void);
uint16_t return_dpmu_sys_freq(void);
uint8_t return_dpmu_internet_protocol(void);



#endif /* INCLUDE_PARAMETERS_DECODER_H_ */
