/*
 * compute_crc16.h
 *
 *  Created on: 17 de jun de 2018
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_COMPUTE_CRC16_H_
#define INCLUDE_COMPUTE_CRC16_H_
#include <stdlib.h>

	uint compute_crc16(unsigned char *message, uint MessLen);

#endif /* INCLUDE_COMPUTE_CRC16_H_ */
